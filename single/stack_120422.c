#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Data {
    char judul[50];
    short halaman;
    float berat;
    struct Data *next;
};

struct Data *head = NULL;

void insertBook() {
    struct Data *temp = (struct Data*)malloc(sizeof(struct Data));

    printf("\nMasukkan data buku:\n");
    printf("Judul\t: ");
    getchar();
    fgets(temp->judul, 50, stdin);

    printf("Halaman\t: ");
    scanf("%d", &temp->halaman);

    printf("Berat\t: ");
    scanf("%f", &temp->berat);

    temp->next = head;
    head = temp;
}

void pickBook() {
    struct Data *delete = head;

    if(head == NULL) printf("\nTumpukan kosong. Isi data dulu!\n");
    else {
        printf("\nBuku yang diambil:\n");
        printf("Judul\t: %sHalaman\t: %d\nBerat\t: %.2f\n", delete->judul, delete->halaman, delete->berat);
        head = head->next;
        free(delete);
    }  
}

void showBook() {
    struct Data *show = head;

    if(head == NULL) printf("\nTumpukan kosong. Isi data dulu!\n");
    else {
        printf("\nTumpukan buku:\n");
        while(show != NULL) {
            printf("Judul\t: %sHalaman\t: %d\nBerat\t: %.2f\n\n", show->judul, show->halaman, show->berat);
            show = show->next;
        }
    }
}

int main() {
    char choice;

    start:
    system("cls");
    printf(
        "===== PROGRAM TUMPUKAN BUKU =====\n"
        "=================================\n\n"
    );
    printf("Menu:\n1. Tambah Buku\n2. Ambil Buku\n3. Lihat Buku\n4. Keluar\n");
    insert_choice:
    printf("Masukkan pilihan: ");
    scanf(" %c", &choice);

    switch(choice) {
        case '1':
            insertBook();
            getchar(); getchar();
            goto start;
            break;
        case '2':
            pickBook();
            getchar(); getchar();
            goto start;
            break;
        case '3':
            showBook();
            getchar(); getchar();
            goto start;
            break;
        case '4':
            printf("\n");
            insert_exit:
            printf("Yakin keluar? (y/n): ");
            scanf(" %c", &choice);
            if(choice == 'y' || choice == 'Y') exit(0);
            else if(choice == 'n' || choice == 'N') goto start;
            else goto insert_exit;
            break;
        default:
            goto insert_choice;
            break;
    }
    

    return 0;
}