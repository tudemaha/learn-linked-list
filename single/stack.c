#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Data {
    int ukuran;
    char merk[20];
    struct Data *next;
};

struct Data *head = NULL;

void insertData() {
    struct Data *temp = (struct Data*)malloc(sizeof(struct Data));

    printf("\nMasukkan data baju:\n");
    printf("Ukuran baju\t: ");
    scanf("%d", &temp->ukuran);

    printf("Merk baju\t: ");
    getchar();
    fgets(temp->merk, 20, stdin);

    temp->next = head;
    head = temp;
}

void pickData() {
    struct Data *temp = head;
    if(head == NULL) {
        printf("\nData baju kosong!");
    } else{
        printf("\nBaju diambil:\n");
        printf("Ukuran baju\t: %d\nMerk baju\t: %s", temp->ukuran, temp->merk);
        head = temp->next;
        free(temp);
    }
    
}

void showData() {
    struct Data *temp = head;
    if(temp == NULL) printf("\nData baju kosong!\n");
    else {
        printf("\nData tumpukan baju:\n");
        while(temp != NULL) {
            printf("Ukuran baju\t: %d\nMerk baju\t: %s\n", temp->ukuran, temp->merk);
            temp = temp->next;
        }
    }
}

int main() {
    char choice;

    start:
    system("clear");
    printf(
        "===== PROGRAM TUMPUKAN BAJU =====\n"
        "=================================\n\n"
    );

    printf("Menu:\n1. Tambah\n2. Ambil\n3. Cek\n4. Keluar\n");
    insert_choice:
    printf("Masukkan pilihan: ");
    scanf(" %c", &choice);

    switch(choice) {
        case '1':
            insertData();
            getchar();
            goto start;
            break;
        case '2':
            pickData();
            getchar(); getchar();
            goto start;
            break;
        case '3':
            showData();
            getchar(); getchar();
            goto start;
            break;
        case '4':
            printf("\n");
            insert_exit:
            printf("Yakin keluar? (y/n): ");
            scanf(" %c", &choice);
            if(choice == 'y' || choice == 'Y') exit(0);
            else if(choice == 'n' || choice == 'N') goto start;
            else goto insert_exit;
            break;
        default:
            goto insert_choice;
    }
    

    return 0;
}