#include <stdio.h>
#include <stdlib.h>

struct Data{
    int nomor;
    struct Data *next;
};

struct Data *front = NULL;
struct Data *rear = NULL;

void insertData() {
    struct Data *temp = (struct Data*)malloc(sizeof(struct Data));

    printf("\nMasukkan nomor antrean: ");
    scanf("%d", &temp->nomor);
    temp->next = NULL;

    if(front == NULL && rear == NULL) {
        front = rear = temp;
    } else {
        rear->next = temp;
        rear = temp;
    }
}

void pickData() {
    struct Data *delete = front;

    if(front == NULL && rear == NULL) {
        printf("\nAntrean kosong. Tambah antrean dulu!\n");
    } else if(front == rear) {
        printf("\nNomor antrean dipanggil: %d\n", front->nomor);
        front = rear = NULL;
        free(delete);
    } else {
        printf("\nNomor antrean dipanggil: %d\n", front->nomor);
        front = front->next;
        free(delete);
    }
}

void showData() {
    struct Data *temp = front;
    
    if(front == NULL && rear == NULL) {
        printf("\nAntrean kosong. Tambah antrean dulu!\n");
    } else {
        printf("\nDaftar antrean:\n");
        while(temp != NULL) {
            printf("%d ", temp->nomor);
            temp = temp->next;
        }
    }
}

int main() {
    char choice;

    start:
    system("clear");
    printf(
        "===== PROGRAM ANTREAN SEMBAKO =====\n"
        "===================================\n\n"
    );

    printf("Menu:\n1. Tambah\n2. Panggil\n3. Lihat\n4. Keluar\n");
    insert_choice:
    printf("Masukkan pilihan: ");
    scanf(" %c", &choice);

    switch(choice) {
        case '1':
            insertData();
            getchar(); getchar();
            goto start;
            break;
        case '2':
            pickData();
            getchar(); getchar();
            goto start;
            break;
        case '3':
            showData();
            getchar(); getchar();
            goto start;
            break;
        case '4':
            printf("\n");
            insert_exit:
            printf("Yakin keluar? (y/n): ");
            scanf(" %c", &choice);
            if(choice == 'y' || choice == 'Y') exit(0);
            else if(choice == 'n' || choice == 'N') goto start;
            else goto insert_exit;
            break;
        default:
            goto insert_choice;
            break;
    }
    

    return 0;
}