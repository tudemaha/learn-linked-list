#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Data {
    char nama[20];
    struct Data *next;
};

struct Data *head = NULL;

void insertData(char nama[]) {
    struct Data *temp = (struct Data*)malloc(sizeof(struct Data));
    strcpy(temp->nama, nama);
    temp->next = head;
    head = temp;
}

void showData() {
    struct Data *temp = head;
    while(temp != NULL) {
        puts(temp->nama);
        temp = temp->next;
    }
}

int main() {
    insertData("Agung");
    insertData("Made");
    insertData("Kaori");
    insertData("Degung");
    showData();

    // insert after kaori
    printf("\n");
    struct Data *temp = head;
    while(strcmp(temp->nama, "Kaori") != 0) {
        temp = temp->next;
    }
    struct Data *databaru = (struct Data*)malloc(sizeof(struct Data));
    strcpy(databaru->nama, "Naruto");
    databaru->next = temp->next;
    temp->next = databaru;
    showData();

    // insert before made
    printf("\n");
    temp = head;
    struct Data *before = head;
    struct Data *baru = (struct Data*)malloc(sizeof(struct Data));
    while(strcmp(temp->nama, "Made") != 0) {
        before = temp;
        temp = temp->next;
    }
    strcpy(baru->nama, "Andre");
    baru->next = temp;
    before->next = baru;
    showData();

    // delete Naruto
    printf("\n");
    temp = head;
    struct Data *delete = head;
    while(strcmp(temp->nama, "Naruto") != 0) {
        delete = temp;
        temp = temp->next;
    }
    delete->next = temp->next;
    showData();

    return 0;
}