#include <stdio.h>
#include <stdlib.h>

struct Data {
    int nilai;
    struct Data *next;
};

struct Data *head = NULL;

int main() {
    // insert di paling kiri
    struct Data *mahasiswa1 = (struct Data*)malloc(sizeof(struct Data));
    mahasiswa1->nilai = 10;
    mahasiswa1->next = NULL;
    head = mahasiswa1;

    struct Data *mahasiswa2 = (struct Data*)malloc(sizeof(struct Data));
    mahasiswa2->nilai = 20;
    mahasiswa2->next = mahasiswa1;
    head = mahasiswa2;

    struct Data *mahasiswa3 = (struct Data*)malloc(sizeof(struct Data));
    mahasiswa3->nilai = 30;
    mahasiswa3->next = mahasiswa2;
    head = mahasiswa3;

    // insert data di kanan 20
    struct Data*mahasiswa4 = (struct Data*)malloc(sizeof(struct Data));
    mahasiswa4->nilai = 15;
    mahasiswa4->next = mahasiswa1;
    mahasiswa2->next = mahasiswa4;

    // insert data di akhir
    struct Data *temp = head;
    while(temp->next != NULL) temp = temp->next;
    struct Data *mahasiswa5 = (struct Data*)malloc(sizeof(struct Data));
    mahasiswa5->nilai = 5;
    mahasiswa5->next = NULL;
    temp->next = mahasiswa5;

    // hapus data nilai 20
    temp = head;
    struct Data *delete = head;
    while(temp->nilai != 20) {
        delete = temp;
        temp = temp->next;
    }
    delete->next = temp->next;
    free(temp);

    
    // print semua nilai
    temp = head;
    printf("daftar nilai:\n");
    while(temp != NULL) {
        printf("%d ", temp->nilai);
        temp = temp->next;
    }

    return 0;
}