#include <stdio.h>
#include <stdlib.h>

struct Data {
    int nomor;
    struct Data *prev;
    struct Data *next;
};

struct Data *head = NULL;
struct Data *tail = NULL;

int main() {
    // insert di paling kiri
    struct Data *data1 = (struct Data*)malloc(sizeof(struct Data));
    data1->nomor = 10;
    data1->prev = NULL;
    data1->next = NULL;
    head = data1;
    tail = data1;

    struct Data *data2 = (struct Data*)malloc(sizeof(struct Data));
    data2->nomor = 20;
    data2->next = data1;
    data2->prev = NULL;
    data1->prev = data2;
    head = data2;

    struct Data *data3 = (struct Data*)malloc(sizeof(struct Data));
    data3->nomor = 30;
    data3->next = data2;
    data3->prev = NULL;
    data2->prev = data3;
    head = data3;

    // insert setelah nilai 20
    struct Data *data4 = (struct Data*)malloc(sizeof(struct Data));
    data4->nomor = 15;
    data4->next = data1;
    data4->prev = data2;
    data1->prev = data4;
    data2->next = data4;
    
    // insert data setelah 15
    struct Data *temp = head;
    struct Data *data5 = (struct Data*)malloc(sizeof(struct Data));
    while(temp->nomor != 15) temp = temp->next;
    data5->nomor = 12;
    data5->prev = temp;
    data5->next = temp->next;
    temp->next->prev = data5;
    temp->next = data5;

    // hapus data 15
    temp = head;
    while(temp->nomor != 15) temp = temp->next;
    temp->next->prev = temp->prev;
    temp->prev->next = temp->next;
    free(temp);


    struct Data *temp_head = head;
    struct Data *temp_tail = tail;
    while(temp_head != NULL) {
        printf("%d ", temp_head->nomor);
        temp_head = temp_head->next;

    }
    printf("\n");
    while(temp_tail != NULL) {
        printf("%d ", temp_tail->nomor);
        temp_tail = temp_tail->prev;

    }
    return 0;
}