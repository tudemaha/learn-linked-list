#include <stdio.h>
#include <stdlib.h>

struct Data {
    int nomor;
    struct Data *next;
};

struct Data *head = NULL;
struct Data *tail = NULL;

int main() {
    // insert data di kiri
    struct Data *data1 = (struct Data*)malloc(sizeof(struct Data));
    data1->nomor = 10;
    data1->next = NULL;
    tail = data1;
    head = data1;
    tail->next = head;

    struct Data *data2 = (struct Data*)malloc(sizeof(struct Data));
    data2->nomor = 20;
    data2->next = data1;
    head = data2;
    tail->next = head;

    struct Data *data3 = (struct Data*)malloc(sizeof(struct Data));
    data3->nomor = 30;
    data3->next = data2;
    head = data3;
    tail->next = head;

    // insert setelah 20

    // hapus data 20


    // print data
    struct Data *temp = head;
    do {
        printf("%d ", temp->nomor);
        temp = temp->next;
    } while(temp != head);
    

    return 0;
}